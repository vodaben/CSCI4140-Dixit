var urllib = require('urllib');
var Threads= require('webworker-threads');
var keys = {api_key: "4d9ce6cdb5dd8dc917805e16175d7182"};
var url = 'http://en.wikipedia.org/wiki/Special:Random';
var exports = module.exports = {};

exports.randPic = function(cb){
	var callback = cb;
	urllib.request('http://en.wikipedia.org/wiki/Special:Random', function (err, data, res){
		if(res.statusCode == 302){
			var key = res.headers.location.split('/');
			var title = key[key.length-1].split("_");
			var rand = Math.floor(Math.random() * title.length);
			(function(){
				urllib.request('https://api.flickr.com/services/rest/?&method=flickr.photos.search&api_key=4d9ce6cdb5dd8dc917805e16175d7182&format=json&nojsoncallback=1&tags='+title[rand], function (err, data, res){
					try{
						var result = JSON.parse(data.toString());
						var rand = Math.round(Math.random() * result.photos.photo.length);
						callback('https://farm'+result.photos.photo[rand].farm+'.staticflickr.com/'+result.photos.photo[rand].server+'/'+result.photos.photo[rand].id+'_'+result.photos.photo[rand].secret+'_z.jpg');
					}catch(e){
						exports.randPic(callback);
					}
				});
			})();
			return;
		}else{
			exports.randPic(callback);
		}
	});
}