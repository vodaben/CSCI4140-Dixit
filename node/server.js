var colors = require('colors');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var cookies = {};
var ObjectId = require('mongodb').ObjectID;
var MongoClient = require('mongodb').MongoClient
  , assert = require('assert');
var randPicture = require("./testworker.js");

// Connection URL
var url = 'mongodb://dixit:dixit@localhost:27017/dixit';
var db, users, token;


app.get('/', function(req, res){
	res.sendfile('index.html');
});

Array.prototype.shuffle = function() {
    var input = this;
     
    for (var i = input.length-1; i >=0; i--) {
     
        var randomIndex = Math.floor(Math.random()*(i+1)); 
        var itemAtIndex = input[randomIndex]; 
         
        input[randomIndex] = input[i]; 
        input[i] = itemAtIndex;
    }
    return input;
}

var User = function(object, socket){
	var object = object;
	var socket = socket;
	this.getID = function(){return object._id};
	this.getUser = function(){return object};
	this.getSocket = function(){return socket};
}

var Room = function(type, name, size, pass){
	var ruid = new ObjectId();
	var type = type;
	var name = name;
	var size = size;
	var pass = pass;
	var admin;
	var users = [];
	var ready = {};
	var game = null;
	this.addUser = function(user){users.push(user); ready[user.getUser()._id] = false; admin = users[0]};
	this.removeUser = function(user){
		users.splice(users.indexOf(user),1); 
		if(admin == user) admin = users[0];
		delete ready[user.getUser()._id];
	};
	this.ready = function(user){ready[user.getUser()._id] = !ready[user.getUser()._id]};
	this.setGame = function(g){game = g};
	this.getGame = function(){return game};
	this.removeGame = function(){game = null};
	this.getRoom = function(){
		var userlist = {};
		for(var i = 0; i < users.length; i++){
			var user = users[i].getUser();
			userlist[user._id] = {name:user.user, score:user.score};
		}
		var auth = true;
		if(pass == undefined || pass == '') auth = false;
		return {ruid:ruid, name:name, type:type, size:size, admin:admin, users:userlist, auth:auth};
	};
	this.getUsers = function(){
		var userlist = {};
		for(var i = 0; i < users.length; i++){
			var user = users[i].getUser();
			userlist[user._id] = {name:user.user, score:user.score};
		}
		return {list:userlist, ready:ready};
	}
	this.getType = function(){return type};
	this.getName = function(){return name};
	this.getRawUsers = function(){return users};
	this.getAdmin = function(){return admin};
	this.getRUID = function(){return ruid};
	this.verify = function(password){return pass == password};
}

var Player = function(user){
	var user = user;
	var cards = [];
	var point = 0;
	var state = 0;
	this.gain = function(p){point+=p; return (point > 30)};
	this.getState = function(){return state};
	this.addCard = function(c){cards.push(c)};
	this.getCards = function(){return cards};
	this.popCard = function(c){cards.splice(cards.indexOf(c), 1); return c};
	this.getPoint = function(){return point};
	this.getUser = function(){return user.getUser()};
	this.getPlayer = function(){return {id:user.getID(), user:user.getUser(), cards: cards, point: point, state: state}};
	this.getPlayerInfo = function(){return {id:user.getID(), user:user.getUser(), point: point}};
	this.debug = function(){
		console.log(user);
		console.log(cards);
		console.log(point);
	}
}

var Game = function(room){
	var ruid = room.getRUID();
	var type = room.getType();
	var name = room.getName();
	var cards = [];
	var round = 0;
	var player = {};
	var storyteller;
	var story;
	var card;
	var count = 0;
	var voteCount = 0;
	var matching = {};
	this.debug = function(){
		for(var p in player){
			console.log("Player " + player[p] + " : " + player[p].getPoint() + "state : " + player[p].getState());
		}
	}
	this.debugCard = function(){
		for(var i = 0; i < cards.length; i++){
			console.log('<img src="'+cards[i]+'">');
		}
	}
	this.addPlayer = function(p){
		player[p.getUser()._id] = p;
	}
	this.removePlayer = function(p){
		delete player[p.getUser()._id];
	}
	this.removePlayerById = function(p){
		delete player[p];
	}
	this.getPlayer = function(id){
		return player[id];
	}
	this.clearPlayer = function(){
		for(var p in player){
			delete player[p];
		}
	}
	this.getRawPlayer = function(){
		return player;
	}
	this.getPlayerList = function(){
		var list = [];
		for(var p in player){
			list.push(player[p].getPlayerInfo());
		}
		return list;
	}
	this.addCard = function(c){
		cards.push(c);
	}
	this.getCard = function(){
		return card;
	}
	this.pickStoryteller = function(){
		storyteller = player[Object.keys(player)[round % Object.keys(player).length]];
	}
	this.getStoryteller = function(){
		return storyteller;
	}
	this.getName = function(){
		return name;
	}
	this.drawCard = function(){
		cards.shuffle();
		return cards.pop();
	}
	this.fillCardBuffer = function(){
		var that = this;
		for(var i = 0; i < 6; i++){
			(function(){
				randPicture.randPic(that.addCard);
			})();
		}
	}
	this.story = function(c, s){
		matching = {};
		matching[c] = {player:storyteller, guessed:[], isTruth:true};
		card = c;
		story = s;
		return story;
	}
	this.getStory = function(){
		return story;
	}
	this.fake = function(player, card){
		matching[card] = {player:player, guessed:[], isTruth:false};
	}
	this.emptyMatching = function(){
		matching = {};
	}
	this.getMatching = function(){
		return matching;
	}
	this.vote = function(player, card){
		matching[card].guessed.push(player);
		voteCount++;
		return (voteCount == this.getPlayerList().length-1);
	}
	this.trigger = function(){
		count++;
		return (count == this.getPlayerList().length);
	}
	this.resetCount = function(){
		count = 0;
	}
	this.round = function(){
		var score = [];
		if(matching[card].guessed.length == 0){
			console.log("None of the player guessed correctly!");
			// None
			score.push({player:storyteller.getUser(), addition:0, reason: "No one can guess it!"});
			for(var m in matching){
				var thisPlayer = matching[m].player;
				var thisPlayerScore = {player:thisPlayer.getUser(), addition:0, reason: "Your card is voted!"};
				for(var i = 0; i < matching[m].guessed.length; i++){
					thisPlayerScore.addition++;
				}
				thisPlayer.gain(thisPlayerScore.addition);
				score.push(thisPlayerScore);
			}
		}else if(matching[card].guessed.length == Object.keys(player).length-1){
			console.log("All of the player guessed correctly!");
			// All
			score.push({player:storyteller.getUser(), addition:0, reason: "Everyone can guess it!"});
			for(var i = 0; i < matching[card].guessed.length; i++){
				var thisPlayer = matching[card].guessed[i];
				thisPlayer.gain(2);
				score.push({player:thisPlayer.getUser(), addition:2, reason: "You beat the storyteller"});
			}
		}else{
			console.log("Some of the player guessed correctly!");
			// Some
			score.push({player:storyteller.getUser(), addition:3, reason: "Someone can guess it!"});
			storyteller.gain(3);
			for(var i = 0; i < matching[card].guessed.length; i++){
				var thisPlayer = matching[card].guessed[i];
				thisPlayer.gain(3);
				score.push({player:thisPlayer.getUser(), addition:3, reason: "You are correct!"});
			}
			for(var m in matching){
				if(m != card){
					var thisPlayer = matching[m].player;
					var thisPlayerScore = {player:thisPlayer.getUser(), addition:0, reason: "Your card is voted!"};
					for(var i = 0; i < matching[m].guessed.length; i++){
						thisPlayerScore.addition++;
					}
					thisPlayer.gain(thisPlayerScore.addition);
					score.push(thisPlayerScore);
				}
			}
		}
		var playersOverflow = this.getRawPlayer();
		for(var p in playersOverflow){
			if(type == 'c' && playersOverflow[p].getPoint() > 30){
				var remainder = playersOverflow[p].getPoint() % 30;
				playersOverflow[p].gain(0-(remainder)-(remainder));
				score.push({player:playersOverflow[p].getUser(), overflow:remainder});
			}
		}
		round++;
		count = 0;
		voteCount = 0;
		this.pickStoryteller();
		return score;
	}
	this.isGameEnd = function(){
		for(var p in player){
			if(type == 'gg' && player[p].getPoint() >= 30){
				return true;
			}else if(type == 'c' && player[p].getPoint() == 30){
				return true;
			}
		}
		return false;
	}
}

var online = [];
var rooms = {};

MongoClient.connect(url, function(err, database) {
	assert.equal(null, err);
	logEvent('INFO', 'Database', "Connected correctly to server");
	db = database;
	users = db.collection('users');
	token = db.collection('token');

	//START SOCKET
	io.on('connection', function(socket){
		var user, room, game;

		logEvent('INFO', 'Connection', 'Established: '+ socket.id);
		socket.emit('authentication', {type: 'synchronize'});
		socket.on('authentication', function(action){
			switch(action.type){
				case 'login':
					logEvent('INFO', 'Authentication', 'Client '+ socket.id+ ': user '+ action.user+ ' password '+ action.pass);
					users.find({user:action.user}).toArray(function(err,uarr){
						if(uarr.length <= 0){
							logEvent('CRIT', 'Authentication', 'Client '+socket.id+' user not exist');
							socket.emit('authentication', {type:'login', message:'Authentication failed'});
							return;
						}
						if(uarr[0].pass != action.pass){
							logEvent('CRIT', 'Authentication', 'Client '+socket.id+' password error');
							socket.emit('authentication', {type:'login', message:'Authentication failed'});
							return;
						}
						var tuser = uarr[0];
						token.find({user:action.user}).toArray(function(err,tarr){
							if(tarr.length > 0){
								var usertoken = tarr[0];
								usertoken._id = new ObjectId();
								for(var i = 0; i < online.length; i++){
									if(tuser._id.toString() == online[i].getID().toString()){
										logEvent('CRIT', 'Authentication', 'Client '+socket.id+' attempt re-authentication');
										socket.emit('authentication', {type:'login', message:'Re-login is not allowed!'});
										return;
									}
								}
								logEvent('SUCC', 'Authentication', 'Client '+socket.id+' logged in with: '+tuser.user);
								token.remove({user:action.user});
								token.insert(usertoken);
								user = new User(uarr[0], socket.id);
								online.push(user);
								//socket.emit('authentication', {type:'debug'});
								socket.emit('authentication', {type:'authenticate', id:usertoken._id, user:user.getUser()});
								var onlineplayers = [];
								for(var i = 0; i < online.length; i++){
									var u = online[i].getUser();
									onlineplayers.push({name:u.user, score:u.score});
								}
								io.sockets.emit('lobby', {type:'playerlist', list:onlineplayers});
							}else{
								logEvent('SUCC', 'Authentication', 'Client '+socket.id+' logged in with: '+tuser.user);
								var usertoken = {_id: new ObjectId(), user:action.user};
								token.insert(usertoken);
								user = new User(uarr[0], socket.id);
								online.push(user);
								socket.emit('authentication', {type:'authenticate', id:usertoken._id, user:user.getUser()});
								var onlineplayers = [];
								for(var i = 0; i < online.length; i++){
									var u = online[i].getUser();
									onlineplayers.push({name:u.user, score:u.score});
								}

								io.sockets.emit('lobby', {type:'playerlist', list:onlineplayers});
							}
						});
					});
					break;
				case 'token':
					logEvent('INFO', 'Authentication', 'Client '+ socket.id+ ': token '+ action.token);
					token.find({"_id":ObjectId(action.token)}, {explain:false}).toArray(function(err, arr) {
						if(arr.length > 0){
							logEvent('SUCC', 'Authentication', 'Client '+socket.id+' authenticated: '+arr[0].user);
							users.find({user:arr[0].user}).toArray(function(err, arr) {
								user = new User(arr[0], socket.id);
								for(var i = 0; i < online.length; i++){
									if(arr[0]._id.toString() == online[i].getID().toString()){
										logEvent('CRIT', 'Authentication', 'Client '+socket.id+' attempt re-authentication with token');
										socket.emit('authentication', {type:'login', message:'Re-login is not allowed!'});
										return;
									}
								}
								online.push(user);
								socket.emit('authentication', {type:'authenticate', id:action.token, user:user.getUser()});
								var onlineplayers = [];
								for(var i = 0; i < online.length; i++){
									var u = online[i].getUser();
									onlineplayers.push({name:u.user, score:u.score});
								}
								socket.broadcast.emit('lobby', {type:'playerlist', list:onlineplayers});
							});
						}else{
							logEvent('CRIT', 'Authentication', 'Client '+socket.id+' token invalid');
							socket.emit('authentication', {type:'login', message:'Invalid token'});
						}
					});
					break;
				case 'logout':
					logEvent('SUCC', 'Authentication', 'Client '+ socket.id+ ': Logout');
					online.splice(online.indexOf(user), 1);
					token.remove({user:user.getUser().user});
					if(room){
						room.removeUser(user);
						socket.leave(room.getRUID());
						io.to(room.getRUID()).emit('room', {type:'refresh'});
						if(Object.keys(room.getUsers().list).length <= 0){
							var r = room.getRoom();
							logEvent('INFO', 'Room ('+r.name+')', 'Room destroying');
							delete rooms[room.getRUID()];
							room = null;
							var roomList = [];
							for(var r in rooms){
								var roomInfo = rooms[r].getRoom();
								roomList.push({ruid:r, name:roomInfo.name, size:roomInfo.size, type:roomInfo.type, userSize:Object.keys(roomInfo.users).length, auth:roomInfo.auth});
							}
							io.sockets.emit('lobby', {type:'roomlist', list:roomList});
						}
					}
					user = null;
					socket.broadcast.emit('lobby', {type:'newPlayerPushed'});
					socket.emit('authentication', {type:'logout'});
					socket.emit('authentication', {type:'login', message:'You are logged out'});
					var onlineplayers = [];
					for(var i = 0; i < online.length; i++){
						var u = online[i].getUser();
						onlineplayers.push({name:u.user, score:u.score});
					}
					io.sockets.emit('lobby', {type:'playerlist', list:onlineplayers});
					break;
			}
		});

		socket.on('registration', function(action){
			switch(action.type){
				case 'register':
					logEvent('INFO', 'Registration', 'Client '+ socket.id+ ': user '+ action.user+ ' password '+ action.pass);
					users.find({user:action.user}).toArray(function(err,arr){
						if(arr.length > 0){
							logEvent('CRIT', 'Registration', 'Client '+socket.id+' registering user ' + action.user + ' exist');
							socket.emit('authentication', {type:'login', message:'User exist!'});
						}else{
							users.insert({"user": action.user, "pass": action.pass, "score": 0}, function(err,r){
	    						logEvent('SUCC', 'Registration', 'Client '+socket.id+' created user: '+action.user);
								socket.emit('authentication', {type:'login', message:'User '+action.user+' created!'});
							});
						}
					});	
					break;
			}
		});

		socket.on('lobby', function(action){
			switch(action.type){
				case 'getList':
					logEvent('INFO', 'Lobby', 'Client '+ socket.id+ ': get lists');
					var onlineplayers = [];
					for(var i = 0; i < online.length; i++){
						var u = online[i].getUser();
						onlineplayers.push({name:u.user, score:u.score});
					}
					socket.emit('lobby', {type:'playerlist', list:onlineplayers});
					var roomList = [];
					for(var r in rooms){
						var roomInfo = rooms[r].getRoom();
						roomList.push({ruid:r, name:roomInfo.name, size:roomInfo.size, type:roomInfo.type, userSize:Object.keys(roomInfo.users).length, auth:roomInfo.auth});
					}
					socket.emit('lobby', {type:'roomlist', list:roomList});
					break;
				case 'create':
					logEvent('INFO', 'Lobby', 'Client '+ socket.id+ ': create room');
					logEvent('DEBUG', 'Lobby', action.room);
					room = new Room(action.room.type, action.room.name, action.room.size, action.room.pass);
					logEvent('DEBUG', 'Lobby', 'Room type: '+room.getType());
					room.addUser(user);
					rooms[room.getRUID()] = room;
					socket.join(room.getRUID());
					socket.emit('lobby', {type:'join', room:room.getRoom()});
					var roomList = [];
					for(var r in rooms){
						var roomInfo = rooms[r].getRoom();
						roomList.push({ruid:r, name:roomInfo.name, size:roomInfo.size, type:roomInfo.type, userSize:Object.keys(roomInfo.users).length, auth:roomInfo.auth});
					}
					io.sockets.emit('lobby', {type:'roomlist', list:roomList});
					break;
				case 'join':
					logEvent('INFO', 'Lobby', 'Client '+ socket.id+ ': join room');
					logEvent('DEBUG', 'Lobby', "Room: " + action.room);
					if(rooms[action.room]){
						logEvent('DEBUG', 'Lobby', "Room exist");
						var r = rooms[action.room].getRoom();
						if(r.auth && rooms[action.room].verify(action.pass)){
							logEvent('DEBUG', 'Lobby', "Authenticated");
							rooms[action.room].addUser(user);
							room = rooms[action.room];
							socket.join(room.getRUID());
							socket.emit('lobby', {type:'join', room:r});
							var roomList = [];
							for(var r in rooms){
								var roomInfo = rooms[r].getRoom();
								roomList.push({ruid:r, name:roomInfo.name, size:roomInfo.size, type:roomInfo.type, userSize:Object.keys(roomInfo.users).length, auth:roomInfo.auth});
							}
							io.sockets.emit('lobby', {type:'roomlist', list:roomList});
						}else if(!r.auth){
							logEvent('DEBUG', 'Lobby', "Joined");
							rooms[action.room].addUser(user);
							room = rooms[action.room];
							socket.join(room.getRUID());
							socket.emit('lobby', {type:'join', room:r});
							var roomList = [];
							for(var r in rooms){
								var roomInfo = rooms[r].getRoom();
								roomList.push({ruid:r, name:roomInfo.name, size:roomInfo.size, type:roomInfo.type, userSize:Object.keys(roomInfo.users).length, auth:roomInfo.auth});
							}
							io.sockets.emit('lobby', {type:'roomlist', list:roomList});
						}else{
							logEvent('DEBUG', 'Lobby', "Error");
							socket.emit('lobby', {type:'joinError', message:'Credential error'});
						}
					}else{
						socket.emit('lobby', {type:'joinError', message:'Room error'});
					}
					break;
			}
		});

		socket.on('room', function(action){
			switch(action.type){
				case 'init':
					var r = room.getRoom();
					logEvent('INFO', 'Room ('+r.name+')', 'Client '+ socket.id+ ': request init');
					socket.emit('room', {type:'init', room:r});
					io.to(room.getRUID()).emit('room', {type:'refresh'});
					break;
				case 'getUserlist':
					var r = room.getRoom();
					logEvent('INFO', 'Room ('+r.name+')', 'Client '+ socket.id+ ': request userlist');
					var admin = false;
					if(room.getAdmin().getUser()._id == user.getUser()._id) admin = true;
					socket.emit('room', {type:'userlist', users:room.getUsers(), admin:room.getAdmin().getUser(), isAdmin:admin, user:user.getUser()._id});
					break;
				case 'ready':
					var r = room.getRoom();
					logEvent('INFO', 'Room ('+r.name+')', 'Client '+ socket.id+ ': ready');
					room.ready(user);
					var admin = false;
					if(room.getAdmin().getUser()._id == user.getUser()._id) admin = true;
					socket.emit('room', {type:'userlist', users:room.getUsers(), admin:room.getAdmin().getUser(), isAdmin:admin, user:user.getUser()._id});
					io.to(room.getRUID()).emit('room', {type:'refresh'});
					break;
				case 'leave':
					var r = room.getRoom();
					logEvent('INFO', 'Room ('+r.name+')', 'Client '+ socket.id+ ': leave room');
					socket.leave(room.getRUID());
					room.removeUser(user);
					io.to(room.getRUID()).emit('room', {type:'refresh'});
					if(Object.keys(room.getUsers().list).length <= 0){
						logEvent('INFO', 'Room ('+r.name+')', 'Room destroying');
						delete rooms[room.getRUID()];
						room = null;
					}
					var roomList = [];
					for(var r in rooms){
						var roomInfo = rooms[r].getRoom();
						roomList.push({ruid:r, name:roomInfo.name, size:roomInfo.size, type:roomInfo.type, userSize:Object.keys(roomInfo.users).length, auth:roomInfo.auth});
					}
					io.sockets.emit('lobby', {type:'roomlist', list:roomList});
					io.to(r.ruid).emit('room', {type:'refresh'});
					break;
				case 'start':
					var r = room.getRoom();
					logEvent('INFO', 'Room ('+r.name+')', 'Client '+ socket.id+ ': start game');
					io.to(room.getRUID()).emit('room', {type:'start'});
					game = new Game(room);
					room.setGame(game);
					delete rooms[room.getRUID()];
					var usersList = room.getRawUsers();
					for(var i = 0; i < usersList.length; i++){
						var newPlayer = new Player(usersList[i]);
						game.addPlayer(newPlayer);
					}

					game.pickStoryteller();
					var randPicCallback = (function(game, length){
						logEvent('DEBUG', 'Room ('+r.name+')', 'Generating pictures');
						var counter = 0;
						var target = length;
						return function(result){
							game.addCard(result);
							counter++;
							if(counter >= target){
								logEvent('DEBUG', 'Room ('+r.name+')', 'Generated');
								io.to(room.getRUID()).emit('room', {type:'game'});
								io.sockets.emit('lobby', {type:'roomlist', list:roomList});
							}
						}
					})(game, usersList.length * 6);
					for(var i = 0; i < usersList.length * 6; i++){
						randPicture.randPic(randPicCallback);
					}
					break;
			}
		});

		socket.on('game', function(action){
			if(action.type != 'gameLeave'){
				game = room.getGame();
				var player = game.getPlayer(user.getID());
				var storyteller = game.getStoryteller();
			}
			if(game.getPlayerList().length < 3){
				try{
					game = room.getGame();
					if(game){
						game.removePlayerById(user.getID());
						logEvent('INFO', 'Game ('+game.getName()+')', 'Game ended because of player leave');
						io.to(room.getRUID()).emit('game', {type:'gameEndException', players: game.getPlayerList()});
						game.emptyMatching();
					}
				}catch(e){console.log(e.stack)};
				return;
			}
			switch(action.type){
				case 'init':
					logEvent('INFO', 'Game ('+game.getName()+')', 'Client '+ socket.id+ ': init');
					player.debug();
					for(var i = 0; i < 6; i++){
						player.addCard(game.drawCard());
					}
					player.debug();
					socket.emit('game', {type:'init', isStoryteller:(user.getID() == storyteller.getUser()._id), player:player.getPlayer(), playerlist:game.getPlayerList()});
					break;
				case 'initRound':
					logEvent('INFO', 'Game ('+game.getName()+')', 'Client '+ socket.id+ ': get round information');
					socket.emit('game', {type:'init', isStoryteller:(user.getID() == storyteller.getUser()._id), player:player.getPlayer(), playerlist:game.getPlayerList()});
					break;
				case 'continue':
					logEvent('INFO', 'Game ('+game.getName()+')', 'Client '+ socket.id+ ': continue');
					player.addCard(game.drawCard());
					if(game.trigger()){
						logEvent('SUCC', 'Game ('+game.getName()+')', 'Triggered continue');
						game.resetCount();
						io.to(room.getRUID()).emit('game', {type:'nextRound'});
					}
					break;
				case 'storytellerTell':

					logEvent('INFO', 'Game ('+game.getName()+')', 'Client '+ socket.id+ ': storyteller told a story');
					var cardTold = player.popCard(action.card);
					logEvent('DEBUG', 'Game ('+game.getName()+')', "Card: "+ cardTold);
					logEvent('DEBUG', 'Game ('+game.getName()+')', "Story: "+ action.story);
					logEvent('DEBUG', 'Game ('+game.getName()+')', player.getCards());
					var story = game.story(cardTold, action.story);
					socket.broadcast.to(room.getRUID()).emit('game', {type:'story', story:action.story});
					if(user.getID() == storyteller.getUser()._id){
						game.fillCardBuffer();
					}
					break;
				case 'guesserFake':
					logEvent('INFO', 'Game ('+game.getName()+')', 'Client '+ socket.id+ ': guesser give a fake');
					var cardTold = player.popCard(action.card);
					logEvent('DEBUG', 'Game ('+game.getName()+')', "Card: "+ cardTold);
					logEvent('DEBUG', 'Game ('+game.getName()+')', player.getCards());
					game.fake(player, action.card);
					var matching = game.getMatching();
					if(Object.keys(matching).length == game.getPlayerList().length){
						var cards = Object.keys(matching);
						cards.shuffle();
						io.to(room.getRUID()).emit('game', {type:'guess', story:game.getStory(), cards:cards});
					}
					break;
				case 'guesserVote':
					logEvent('INFO', 'Game ('+game.getName()+')', 'Client '+ socket.id+ ': guesser give a vote');
					logEvent('DEBUG', 'Game ('+game.getName()+')', "Card: "+ action.card);
					logEvent('DEBUG', 'Game ('+game.getName()+')', player.getCards());
					if(game.vote(player, action.card)){
						logEvent('SUCC', 'Game ('+game.getName()+')', 'All players voted');
						var score = game.round();
						var matching = game.getMatching();
						var story = game.getStory();
						if(game.isGameEnd()){
							logEvent('INFO', 'Game ('+game.getName()+')', 'Game ended');
							for(var m in matching){
								matching[m].player = matching[m].player.getUser();
								for(var i = 0; i < matching[m].guessed.length; i++){
									matching[m].guessed[i] = matching[m].guessed[i].getUser();
								}
							}
							io.to(room.getRUID()).emit('game', {type:'gameEnd', score: score, matching: matching, story: story, players: game.getPlayerList()});
							var playerlist = game.getRawPlayer();
							for(var p in playerlist){
								var u = playerlist[p].getUser();
								var score = playerlist[p].getPoint();
								if(score >= 30){
									u.score += 30 * 2;
								}else if(score >= 10){
									u.score += score;
								}
								users.update({_id:u._id}, u);
							}
						}else{
							logEvent('INFO', 'Game ('+game.getName()+')', 'Round ended');
							for(var m in matching){
								matching[m].player = matching[m].player.getUser();
								for(var i = 0; i < matching[m].guessed.length; i++){
									matching[m].guessed[i] = matching[m].guessed[i].getUser();
								}
							}
							io.to(room.getRUID()).emit('game', {type:'roundScore', score: score, matching: matching, story: story, players: game.getPlayerList()});
						}
						game.emptyMatching();
					}
					break;
				case 'gameLeave':
					logEvent('INFO', 'Game ('+game.getName()+')', 'Distroying game for '+socket.id);
					socket.leave(room.getRUID());
					delete room;
					delete rooms[room.getRUID()];
					game.clearPlayer();
					delete game;
					room.removeGame();
					room = null;
					logEvent('SUCC', 'Game ('+game.getName()+')', 'Distroyed game for '+socket.id);
					game = null;
					break;
			}
		});

		socket.on('disconnect', function(action){
			logEvent('INFO', 'Connection', 'Client '+ socket.id+ ': Disconnected');
			if(room != null){
				try{
					game = room.getGame();
					if(game){
						game.removePlayerById(user.getID());
						logEvent('INFO', 'Game ('+game.getName()+')', 'Game ended because of player leave');
						io.to(room.getRUID()).emit('game', {type:'gameEndException', players: game.getPlayerList()});
						game.emptyMatching();
					}
				}catch(e){console.log(e.stack)};
				room.removeUser(user);
				socket.leave(room.getRUID());
				io.to(room.getRUID()).emit('room', {type:'refresh'});
				if(Object.keys(room.getUsers().list).length <= 0){
					var r = room.getRoom();
					logEvent('INFO', 'Room ('+r.name+')', 'Room destroying');
					delete rooms[room.getRUID()];
					room = null;
					var roomList = [];
					for(var r in rooms){
						var roomInfo = rooms[r].getRoom();
						roomList.push({ruid:r, name:roomInfo.name, size:roomInfo.size, type:roomInfo.type, userSize:Object.keys(roomInfo.users).length, auth:roomInfo.auth});
					}
					io.sockets.emit('lobby', {type:'roomlist', list:roomList});
				}
			}
			if(user != null){
				//userOffline(user);
				if(online.indexOf(user) != -1){
					online.splice(online.indexOf(user),1);
				}
				var onlineplayers = [];
				for(var i = 0; i < online.length; i++){
					var u = online[i].getUser();
					onlineplayers.push({name:u.user, score:u.score});
				}
				socket.broadcast.emit('lobby', {type:'playerlist', list:onlineplayers});
			}
		});

		socket.on('error', function(err){
			console.log(err.stack);
		});
	});

	/*
	try{
		users.find({user:'vodaben'}).toArray(function(err, arr){
			var user = new User(arr[0]);
			var player = new Player(user);
			var object = player.getUser().getUser();
			console.log(object);
			object.score = 0;
			users.update({_id:object._id}, object);
		});
	}catch(err){
		console.log(err);
	}
	*/
});


http.listen(8080, function(){
	logEvent('INFO', 'Server', 'Server listening on *:8080');
});

//do something when app is closing
process.on('exit', exitCleanUp.bind(null,{cleanup:true}));
process.on('SIGINT', exitCleanUp.bind(null, {exit:true}));
process.on('uncaughtException', exitCleanUp.bind(null, {exit:true}));

function logEvent(level, module, message){
	var fg = 'white',bg = 'bgBlue';
	switch(level){
		case 'INFO': bg='bgYellow'; fg='black'; break;
		case 'SUCC': bg='bgGreen'; fg='black'; break;
		case 'CRIT': bg='bgRed'; break;
		case 'DEBUG': bg='bgWhite'; fg='black'; break;
	}
	console.log(colors[bg][fg]('%s %s - %s'), level, module, message);
}

function exitCleanUp(){
	db.close();
	logEvent('INFO', 'Process Termination', 'Killed database connection');
	process.exit();
}


